package wulandari.afifah.aplikasi8

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.SeekBar
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
//import kotlinx.android.synthetic.main.activity_setting.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var preferences: SharedPreferences
    val PREF_NAME = "setting"
    val FIELD_FONT_SIZE_HEADER = "font_size1"
    val FIELD_TEXT_HEADER = "text1"
    val DEF_FONT_SIZE_HEADER = 24
    val DEF_TEXT_HEADER = "StreamingFilm.com"
    val FIELD_FONT_SIZE_TITLE = "font_size2"
    val FIELD_TEXT_TITLE = "text2"
    val DEF_FONT_SIZE_TITLE = 18
    val DEF_TEXT_TITLE = "INSIDE OUT"
    val FIELD_FONT_SIZE_DETAIL = "font_size3"
    val FIELD_TEXT_DETAIL = "text3"
    val DEF_FONT_SIZE_DETAIL = 14
    val DEF_TEXT_DETAIL = "Inventive, gorgeously animated, and powerfully moving, Insid Out is another outstanding addition to the Pixar library of modern animated classics"
    val SETTING: Int = 200

    val onSeekHeader = object : SeekBar.OnSeekBarChangeListener{
        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            txHeader.setTextSize(progress.toFloat())
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {

        }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {

        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        txHeader.setText(preferences.getString(FIELD_TEXT_HEADER, DEF_TEXT_HEADER))
        txHeader.textSize = preferences.getInt(FIELD_FONT_SIZE_HEADER, DEF_FONT_SIZE_HEADER).toFloat()
        txTitle.setText(preferences.getString(FIELD_TEXT_TITLE, DEF_TEXT_TITLE))
        txTitle.textSize = preferences.getInt(FIELD_FONT_SIZE_TITLE, DEF_FONT_SIZE_TITLE).toFloat()
        txIsiDetail.setText(preferences.getString(FIELD_TEXT_DETAIL, DEF_TEXT_DETAIL))
        txIsiDetail.textSize = preferences.getInt(FIELD_FONT_SIZE_DETAIL, DEF_FONT_SIZE_DETAIL).toFloat()
        //sBarFontHeader.progress = preferences.getInt(FIELD_FONT_SIZE_HEADER, DEF_FONT_SIZE_HEADER)
        //sBarFontHeader.setOnSeekBarChangeListener(onSeekHeader)
    }

    // a method to crete OptionsMenu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_option, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item?.itemId) {

            R.id.itemSetting ->{
                //startActivity(Intent(this,SettingActivity::class.java))


               // var intent = Intent(this, SettingActivity::class.java)
                //intent.putExtra("X", edHasil.text.toString()) //send a "X" value to second activity
                startActivityForResult(intent, SETTING)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onClick(v: View?) {
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val prefEditor = preferences.edit()
        prefEditor.putString(FIELD_TEXT_HEADER, txHeader.text.toString())
        prefEditor.putString(FIELD_TEXT_TITLE, txTitle.text.toString())
        prefEditor.putString(FIELD_TEXT_DETAIL, txIsiDetail.text.toString())
        //prefEditor.putInt(FIELD_FONT_SIZE_HEADER, sBarFontHeader.progress)
        prefEditor.commit()
        Toast.makeText(this, "Perubahan Telah Disimpan", Toast.LENGTH_SHORT).show()

    }
}

